<?php

use Illuminate\Support\Facades\Route;
use Shean\HelloWorldPackage\Contracts\Transformer;

Route::get('/hello', function (Transformer $transformer) {
    return view('hello-world::hello', [
        'name' => $transformer->transform(config('hello-world.name')),
    ]);
});