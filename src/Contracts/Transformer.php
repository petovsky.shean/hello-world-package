<?php


namespace Shean\HelloWorldPackage\Contracts;


interface Transformer
{
    /**
     * Transform string and returns it transformed.
     *
     * @param string $string
     * @return string
     */
    public function transform(string $string): string;
}