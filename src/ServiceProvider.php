<?php


namespace Shean\HelloWorldPackage;


use Shean\HelloWorldPackage\Contracts\Transformer;
use Shean\HelloWorldPackage\Transformer\ReverseString;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'hello-world');

        $this->publishes([
            __DIR__ . '/../config/hello-world.php' => config_path('hello-world.php'),
            __DIR__ . '/../resources/views' => resource_path('views/vendor/hello-world'),
        ]);
    }

    public function register()
    {
        $this->app->bind(Transformer::class, ReverseString::class);
    }
}