<?php


namespace Shean\HelloWorldPackage\Transformer;


use Shean\HelloWorldPackage\Contracts\Transformer;

class ReverseString implements Transformer
{
    public function transform(string $string): string
    {
        return strrev($string);
    }
}