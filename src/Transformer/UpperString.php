<?php


namespace Shean\HelloWorldPackage\Transformer;


use Shean\HelloWorldPackage\Contracts\Transformer;

class UpperString implements Transformer
{

    public function transform(string $string): string
    {
        return strtoupper($string);
    }
}